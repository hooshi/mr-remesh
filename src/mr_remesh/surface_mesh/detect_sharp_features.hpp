//
// MR_REMESH/surface_mesh/subdivider.hpp
//
// Author: Shayan Hoshyari
//

#ifndef MR_REMESH_SURFACE_MESH_DETECT_SHARP_FEATURES
#define MR_REMESH_SURFACE_MESH_DETECT_SHARP_FEATURES

#include <vector>

#include <Eigen/Core>

namespace mrremesh
{

namespace surfacemesh
{

class Connectivity;


void
detect_sharp_edges(const double angle_thr,
                   /* const */ Connectivity & connectivity,
                   Eigen::Matrix2i & sharp_edges);

void
detect_sharp_vertices(const double angle_thr,
                   /* const */ Connectivity & connectivity,
                   Eigen::VectorXi & sharp_vertices);

} // end of surfacemesh

} // end of mrremesh


#endif // MR_REMESH_SURFACE_MESH_DETECT_SHARP_FEATURES

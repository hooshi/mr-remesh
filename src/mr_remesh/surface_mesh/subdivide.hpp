//
// mr_remesh/surface_mesh/subdivider.hpp
//
// Author: Shayan Hoshyari
//

#ifndef MR_REMESH_SURFACE_MESH_SUBDIVIDE_IS_INCLUDED
#define MR_REMESH_SURFACE_MESH_SUBDIVIDE_IS_INCLUDED

#include <vector>

#include <Eigen/Core>

namespace mrremesh
{

namespace surfacemesh
{

class Connectivity;



// Subdivides the connectivity structure
void
subdivide(Connectivity & connectivity,
    const Eigen::VectorXi & faces_to_subdivide,
    const int n_levels,
    Eigen::VectorXi & owning_face,
    Eigen::Matrix3d & barycentric_coords);

// Same as before, but the input and output are now face lists
//
// NOTE: For now it actually cheats and uses the previous implementation.
//       However, for this type of connectiviy representation, we can do it much faster.
void
subdivide(const Eigen::Matrix3i & F_in,
    const Eigen::VectorXi & faces_to_subdivide,
    const int n_levels,
    Eigen::Matrix3i & F_out,
    Eigen::VectorXi & owning_face,
    Eigen::Matrix3d & barycentric_coords);

} // end of surfacemesh

} // end of MR_REMESH


#endif // MR_REMESH_SURFACE_MESH_BUILD_CONNECTIVITY_MATRIX_IS_INCLUDED

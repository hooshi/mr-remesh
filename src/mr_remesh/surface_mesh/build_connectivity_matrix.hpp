//
// mr_remesh/surface_mesh/build_connectivity_matrix.hpp
//
// Author: Shayan Hoshyari
//

#ifndef MR_REMESH_SURFACE_MESH_BUILD_CONNECTIVITY_MATRIX_IS_INCLUDED
#define MR_REMESH_SURFACE_MESH_BUILD_CONNECTIVITY_MATRIX_IS_INCLUDED


#include <Eigen/Core>

namespace mrremesh
{

namespace surfacemesh
{

class Connectivity;

Eigen::Matrix3Xi build_connectivity_matrix(/* const */ Connectivity&);

} // end of surfacemesh

} // end of MR_REMESH


#endif  // MR_REMESH_SURFACE_MESH_BUILD_CONNECTIVITY_MATRIX_IS_INCLUDED

//
// mr_remesh/surface_mesh/reference_surface.hpp
//
// Author: Shayan Hoshyari
//

#ifndef MR_REMESH_SURFACE_MESH_REFERENCE_SURFACE
#define MR_REMESH_SURFACE_MESH_REFERENCE_SURFACE

// std cpp
#include <memory>

// eigen
#include <Eigen/Core>

namespace mrremesh
{
namespace surfacemesh
{

// forward decl.
class Connectivity;

//  return types
enum Reference_surface_error_code
{
  REFERENCE_SURFACE_SUCCESS = 0,
  REFERENCE_SURFACE_FAILURE,
  REFERENCE_SURFACE_FATAL_ERROR
};

// concrete class types
enum Reference_surface_type
{
  REFERENCE_SURFACE_GEODESIC_PATCHES = 0,
  REFERENCE_SURFACE_GEODESIC_NAIVE,
  REFERENCE_SURFACE_CLOSEST_POINT,
  REFERENCE_SURFACE_PLANE,
  REFERENCE_SURFACE_CUBE,
  REFERENCE_SURFACE_ELLIPSOID
};

// convert enum to string
std::string
reference_surface_type_to_string(const Reference_surface_type type);

// factory
Reference_surface *
build_reference_surface(const Reference_surface_type type,
    /* const */ Connectivity &,
    const std::string additional_options);


// ===========================================================
//                       ABSTRACT API
// ===========================================================
class Reference_surface
{

public:
  // Children can recognize this class as base
  using Base_class = Reference_surface;

  // Abstract type for vertex data
  class Vertex_data
  {
  public:
    // Children can recognize this class as base
    using Base_class = Vertex_data;
    using Base_upper_class = Reference_surface;

    // Get the vertex normal
    virtual Eigen::Vector3d get_normal() = 0;

    // Get the vertex position
    virtual Eigen::Vector3d get_position() = 0;

    // Get sizing field
    virtual double get_sizing_field() =0;

    // Clone this class
    virtual Base_class * clone() = 0;

    // Clone as a unique ptr
    std::unique_ptr<Base_class::Vertex_data> clone_as_unique_ptr();
  };

  // Project a point onto the reference geometry
  // This point should be specified via a triangle with vertices
  // p0, p1, and p2, and a barycentric coordinate xi.
  // The projected point will be returned as ans.
  virtual Reference_surface_error_code project_point(const Base_class::Vertex_data * p0,
      const Base_class::Vertex_data * p1,
      const Base_class::Vertex_data * p2,
      const Eigen::Vector3d & xi,
      Base_class::Vertex_data *& ans) = 0;

  // Project a point onto the reference geometry
  // This point should be specified via a line with vertices
  // p0, p1, and a barycentric coordinate xi.
  // The projected point will be returned as ans.
  virtual Reference_surface_error_code project_point(const Base_class::Vertex_data * p0,
      const Base_class::Vertex_data * p1,
      const Eigen::Vector2d & xi,
      Base_class::Vertex_data *& ans) = 0;

  virtual ~Reference_surface() = default;
};


} // end of surfacemesh
} // end of MR_REMESH

#endif /* POINT_LOCATOR_HXX_IS_INCLUDED */

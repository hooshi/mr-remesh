#ifndef MINIMESH_SURFACE_MESH_POINT_LOCATOR
#define MINIMESH_SURFACE_MESH_POINT_LOCATOR

#include <Eigen/Core>

namespace mrremesh
{
namespace surfacemesh
{

class Connectivity;

enum Point_locator_error_code
{
  LOCATOR_SUCCESS = 0,
  LOCATOR_FAILURE,
  LOCATOR_FATAL_ERROR
};

// ========================================================
//       Locate points in a 2-D triangle mesh
// ========================================================

class Point_locator_2d
{

public:
  Point_locator_2d(Connectivity &, const Eigen::Matrix2Xd &pos);

  // Look inside a single triangle or edge
  static Point_locator_error_code locate_in_triangle(const Eigen::Vector2d & x,
      const Eigen::Vector2d & p0,
      const Eigen::Vector2d & p1,
      const Eigen::Vector2d & p2,
      const Eigen::Vector3d & xi);

  Point_locator_error_code locate_in_mesh(Eigen::Vector2d x,
      const int tri_guess,
      int & tri_found,
      Eigen::Vector3d & xi) const;

private:
  Connectivity * _connectivity;
  Eigen::Matrix2Xd pos;
};

// ========================================================
//    Locate points on a 1-D polyline (binary search)
// ========================================================

class Point_locator_1d
{

public:
  Point_locator_1d(const Eigen::VectorXd &pos);

  // Look inside a single triangle or edge
  static Point_locator_error_code locate_in_line(const double & x,
      const double& p0,
      const double & p1,
      const Eigen::Vector2d & xi);


  Point_locator_error_code locate_in_polyline(Eigen::Vector2d x,
      int & line_found,
      Eigen::Vector2d & xi) const;

private:
  Eigen::VectorXd pos;
};

} // end of surfacemesh
} // end of minimesh

#endif /* POINT_LOCATOR_HXX_IS_INCLUDED */

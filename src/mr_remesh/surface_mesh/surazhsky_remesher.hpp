//
// mr_remesh/surface_mesh/surazhsky_remesher.hpp
//
// Author: Shayan Hoshyari
//

#ifndef MR_REMESH_SURFACE_MESH_SURAZHSKY_REMESHER
#define MR_REMESH_SURFACE_MESH_SURAZHSKY_REMESHER

// std cpp
#include <memory>
#include <vector>

// eigen
#include <Eigen/Core>

// mrremesh
#include <mr_remesh/surface_mesh/connectivity.hpp>
#include <mr_remesh/surface_mesh/reference_surface.hpp>

namespace mrremesh
{
namespace surfacemesh
{

class Surazhsky_remesher
{

  //
  // Operations
  //

  // =======================================================
  //   Incremental Operations
  // =======================================================

  //
  // Edge flip
  //
  struct Edge_flip_examination_result
  {
    bool should_perform;
    const int edge_id;
    // Connectivity small_patch;
  };
  Edge_flip_examination_result examine_delaunay_edge_flip(/* */);
  void perform_edge_flip(const Edge_flip_examination_result &);

  //
  // Smoothing
  //
  struct Smooth_examination_result
  {
    bool should_perform;
    const int vertex_id;
    std::unique_ptr<Reference_surface::Vertex_data> new_pos;
    // Connectivity small_patch;
  };
  Smooth_examination_result examine_area_based_smoothing(/* */);
  Smooth_examination_result examine_laplacian_smoothing(/* */);
  void perform_smoothing(const Smooth_examination_result &);

  //
  // Collapse
  //
  struct Collapse_examination_result
  {
    bool should_perform;
    const int half_edge_id;
    std::unique_ptr<Reference_surface::Vertex_data> new_pos;
    // Connectivity small_patch;
  };
  Collapse_examination_result examine_collapse(/* */);
  void perform_collapse(const Collapse_examination_result &);

  //
  // Split
  //
  struct Split_examination_result
  {
    bool should_perform;
    const int vertex_id;
    std::unique_ptr<Reference_surface::Vertex_data> new_pos;
    // Connectivity small_patch;
  };
  Split_examination_result examine_split(/* */);
  void perform_split(const Split_examination_result &);



  // =======================================================
  //   Static functions
  // =======================================================

  //
  // Metrics
  //
  static bool s_check_angle_metric(const double cs_theta,
      Eigen::Vector3d & nv0,
      Eigen::Vector3d & nv1,
      Eigen::Vector3d & nv2,
      Eigen::Vector3d & nf);

  static bool s_check_angle_metric(const double cs_theta,
      Eigen::Vector3d & nv0,
      Eigen::Vector3d & nv1,
      Eigen::Vector3d & nv2);

  static bool s_check_delaunay_flippable(const Eigen::Vector2d & v0,
      const Eigen::Vector2d & v1,
      const Eigen::Vector2d & v2,
      const Eigen::Vector2d & v3);

  static Eigen::Vector2d s_laplacian_smoothing_pos(const Eigen::Vector2d & self, const Eigen::Matrix2Xd & neighbours);

  static Eigen::Vector2d s_areabased_smoothing_pos(const Eigen::Vector2d & self,
      const Eigen::Matrix2Xd & neighbours /*, rations */);


public:
  double _angle_metric_cs_theta4;
  double _angle_metric_cs_theta3;
  //
  Connectivity * _working_mesh;
  std::vector<Reference_surface::Vertex_data *> _point_projector_data;
  Reference_surface * _reference_surface;
  //
  // Connectivity      _initial_mesh;
  // Eigen::Matrix3Xd  _initial_positions;
};


} // end of surfacemesh
} // end of mrremesh

#endif /* MR_REMESH_SURFACE_MESH_SURAZHSKY_REMESHER */

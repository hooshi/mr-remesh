//
// subdivider.hpp
//
//
// Author: Shayan Hoshyari
//

#ifndef MR_REMESH_SURFACE_MESH_INCREMENTAL_OPERATOR_IS_INCLUDED
#define MR_REMESH_SURFACE_MESH_INCREMENTAL_OPERATOR_IS_INCLUDED

#include <vector>

namespace mrremesh
{

namespace surfacemesh
{

class Connectivity;

class Incremental_operator
{
public:
  Incremental_operator(Connectivity & mesh_in);
  const Connectivity & mesh() const;


  // split and edge, input: half edge index
  void split_edge(const int he_index);

  //
  void collapse_edge(const int he_index);

  //
  void flip_edge(const int he_index);

  // cute a corner, input: half edge going out of the corner vertex
  void cut_corner(const int he_index);

  // subdivide a face
  void subdivide_face(const int face_index, const std::vector<bool> & is_vertex_new);


  //
  void build_collapse_mini_patch(const int he_index, Connectivity&, std::vector<int> &new2old_vertex);
  void build_collapse_flip_patch(const int he_index, Connectivity&, std::vector<int> &new2old_vertex);
  void build_umbrella_patch(const int he_index, Connectivity&, std::vector<int> &new2old_vertex);

private:
  Connectivity * _mesh;
  Connectivity & mesh();
};

} // end of surfacemesh

} // end of MR_REMESH

#endif // MR_REMESH_SURFACE_MESH_INCREMENTAL_OPERATOR_IS_INCLUDED

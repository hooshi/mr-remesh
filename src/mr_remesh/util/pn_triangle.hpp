//
// pn_triangle.hpp
//
// Author: Shayan Hoshyari
//

#ifndef MR_REMESH_UTIL_PN_TRIANGLE_IS_INCLUDED
#define MR_REMESH_UTIL_PN_TRIANGLE_IS_INCLUDED

#include <vector>

#include <Eigen/Core>

namespace mrremesh
{

namespace util
{

// Position and normal interpolation with a PN-triangle
//
// Normally, you simply use it as
//   Set the control points and normals
//   PN_triangle pn_tri;
//   pn_tri.set_control_points_and_normals()
//   Now get the normal or position at a location you want
//   ... = pn_tri.position(Eigen::Vector2d(u,v));
//   ... = pn_tri.normal(Eigen::Vector2d(u,v));
//
//
// See pn-triangle.svg for more details.
//
// TODO: Do we need dpos_dsomething at some point?
//       At least not now, since I am not getting any derivatives.
class PN_triangle
{

  // Evaluate the position on the triangle at a uv reference coordinate
  Eigen::Vector3d position(Eigen::Vector2d & uv);

  // Evaluate the normal on the triangle at a uv reference coordinate
  Eigen::Vector3d normal(Eigen::Vector2d & uv);

  // Set the control points and normals at the three triangle vertices
  // This must be called to initialize the triangle
  Eigen::Matrix3Xd set_control_points_and_normals(const Eigen::Matrix3d & control_points,
      Eigen::Matrix3d & control_normals);

  // Querry functions to get the normal and position coefficients
  Eigen::Matrix3Xd get_normal_coeffs();
  Eigen::Matrix3Xd get_position_coeffs();

  // Number of normal and position coefficients
  constexpr static int n_normal_coeffs() { return 6; }
  constexpr static int n_position_coeffs() { return 9; }

  //
  // Main formulas as static functions
  //

  // Computes the coeffs, from the control point and normals.
  static void compute_coeffs(const Eigen::Matrix3d & control_points,
      const Eigen::Matrix3d & control_normals,
      Eigen::Matrix3Xd & position_coeffs,
      Eigen::Matrix3Xd & normal_coeffs);

  // Compute the position from the position coeffs
  static Eigen::Vector3d compute_position(const Eigen::Vector2d & uv, Eigen::Matrix3Xd & position_coeffs);

  // Compute the normal from the normal coeffs
  static Eigen::Vector3d compute_normal(const Eigen::Vector2d & uv, Eigen::Matrix3Xd & normal_coeffs);


public:
  Eigen::Matrix3Xd normal_coeffs = Eigen::Matrix3Xd(3, 0);
  Eigen::Matrix3Xd position_coeffs = Eigen::Matrix3Xd(3, 0);
}; // end of PN_triangle

} // end of util

} // end of MR_REMESH


#endif // MR_REMESH_UTIL_PN_TRIANGLE_IS_INCLUDED

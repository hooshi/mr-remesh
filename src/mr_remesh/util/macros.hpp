// Some handy macros
// Author: Shayan Hoshyari

#ifndef MR_REMESH_UTIL_MACROS_IS_INCLUDED
#define MR_REMESH_UTIL_MACROS_IS_INCLUDED

// Use this to get rid of unused variable warnings.
#define MR_REMESH_UNUSED(x) (void)(x)

// You can use this macro as a shorthand for std::stringstream
// example: std::string name = MINIMESH_STR("run_number_" << i << ".vtk");
#define MR_REMESH_STR(X) static_cast<std::ostringstream&>(std::ostringstream().flush() << X).str()

#endif /* MR_REMESH_UTIL_MACROS_IS_INCLUDED */

//
// console_colors.hpp
//
// Gatherer: Shayan Hoshyari
//


#ifndef MR_REMESH_UTIL_CONSOLE_COLORS_IS_INCLUDED
#define MR_REMESH_UTIL_CONSOLE_COLORS_IS_INCLUDED

// Colors for printing to console. 
// It definitely works on Linux and MAC. On windows, sometimes it works, sometimes it doesn't.
// Thanks to
// https://github.com/dthuerck/mapmap_cpu/blob/master/mapmap/header/mapmap.h

namespace mrremesh
{
namespace util
{ 
namespace consolecolors
{
char constexpr * const black= "\033[0;30m";
char constexpr * const darkblue ="\033[0;34m";
char constexpr * const darkgreen="\033[0;32m";
char constexpr * const darkteal="\033[0;36m";
char constexpr * const darkred="\033[0;31m";
char constexpr * const darkpink="\033[0;35m";
char constexpr * const darkyellow="\033[0;33m";
char constexpr * const gray="\033[0;37m";
char constexpr * const darkgray="\033[1;30m";
char constexpr * const blue="\033[1;34m";
char constexpr * const green="\033[1;32m";
char constexpr * const teal="\033[1;36m";
char constexpr * const red="\033[1;31m";
char constexpr * const pink="\033[1;35m";
char constexpr * const yellow="\033[1;33m";
char constexpr * const white="\033[1;37m";
char constexpr * const reset="\033[0m";
  
} // end of consolecolors
} // end of util
} // end of mrremesh
#endif // MR_REMESH_UTIL_CONSOLE_COLORS_IS_INCLUDED

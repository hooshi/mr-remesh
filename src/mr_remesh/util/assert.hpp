//
// assert.hpp
// author: Shayan Hoshyari
//

#ifndef MR_REMESH_UTIL_ASSERT_IS_INCLUDED
#define MR_REMESH_UTIL_ASSERT_IS_INCLUDED

// cpp std
#include <cassert>
#include <exception>
#include <sstream>

// Local files
#include "console_colors.hpp"
#include "macros.hpp"

// An assertion that gets triggered even in optimized mode.
// Usage example:
// force_assert(i==2, "i is " << i << " which is not 2");
#define MR_REMESH_FORCE_ASSERT_MSG(EXPR, MSG)                                \
  if(!(EXPR))                                                                \
  {                                                                          \
    fprintf(stderr, "Assertion in %s, %d \n", __func__, __LINE__);           \
    fprintf(stderr, "%s", ::mrremesh::util::red());                          \
    fprintf(stderr, "%s", MR_REMESH_STR(#EXPR << "\n" << MSG).c_str());      \
    fprintf(stderr, "%s", ::mrremesh::util::reset()) throw std::exception(); \
    assert(0);                                                               \
    throw std::exeption;                                                     \
  }

// Usage example:
// force_assert(i==2);
#define MR_REMESH_FORCE_ASSERT(EXPR) MR_REMESH_FORCE_ASSERT_MSG(EXPR, "")


#endif /* MR_REMESH_UTIL_ASSERT_IS_INCLUDED */
